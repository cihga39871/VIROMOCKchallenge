## Dataset 8: Cryptic virus and low concentration (PFBV and mitovirus on Chenopodium)

### Introduction to the dataset

The real dataset is composed of *Pelargonium flower break virus* (PFBV) and *Chenopodium quinoa mitovirus 1* (CqMV1), a mitovirus from Chenopodium. **The limit tested is the ability to detect a cryptic (persistent) virus**, which is a virus-encoded RdRp-dependent replicating RNA element, localized in the mitochondria. The cryptic virus CqMV1 has a really low frequency (around 0.5%).

The different labs that analyzed the dataset are listed in Table 1.

*Table 1: Participants to the VIROMOCK challenge of Dataset 8.*

| Participant                      	| Institute 	| Country 	| email                                  	|
|----------------------------------	|-----------	|---------	|----------------------------------------	|
| Lucie Tamisier                   	| ULg       	| Belgium 	| <lucie.tamisier@uliege.be>             	|
| Annelies Haegeman, Yoika Foucart 	| ILVO      	| Belgium 	| <annelies.haegeman@ilvo.vlaanderen.be> 	|


The observed values of the different participants are listed in Table 2.

*Table 2: Observed composition of Dataset 8 after analysis by different labs.*

| Institute/Lab | Virus/viroid | Observed   closest NCBI accession | Observed   proportion of viral reads (%)<sup>1</sup>  | Were   you able to detect CqMV1? |
|:-------------:|--------------|-----------------------------------|------------------------------------------|---------------------------------|
|      ILVO     | PFBV         | AJ514833                          | 98.5                                     | yes                             |
|      ILVO     | CqMV1        | MF375475                          | 1.5                                      |                                 |

<sup>1</sup> This number represents the number of virus filtered reads mapped for each accession against the total number of virus filtered reads.


### Comments of different labs while analyzing the dataset

Here you can read some comments the participants had while analyzing the dataset.

No comments yet.

### How to participate to the VIROMOCK challenge

The dataset can be downloaded [here](https://datadryad.org/stash/share/YjRgAl9YKUMUmjlv3DG4PDEfiEK-DH_QbXkRu9Cdqqk).

If you finish your analysis, we encourage you to submit your results through [this Google Sheet](https://docs.google.com/spreadsheets/d/1sPpt9Yiwj5T4NcF34WjiUPDLUvpygvX1dfnnCs1lX2k/edit#gid=0).

The Google Sheet will allow you to share your results in detail. Only the green columns are required. However, we encourage you to give as much information as possible.

After submission of the Google Sheet, your results will be processed and added to Table 2.
