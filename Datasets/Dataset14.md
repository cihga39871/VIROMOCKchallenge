## Dataset 14: *Potato virus Y* (PVY)

The composition of this dataset is completely artificial and consists of viral reads only (no background reads), without SNPs. The composition is shown in Table 1. The percentage of identity between the strains is shown in Table 2.

The dataset can be downloaded [here](https://datadryad.org/stash/share/nuuZz374Hie15x4hXsOnFXQCp5e9wWTVOXdrbVSBeZg).

*Table 1: Composition of dataset 14. For each accession, the "expected proportion of viral reads" represents the number of virus filtered reads mapped for each accession against the total number of virus filtered reads.*

|     References    	|     Type          	|     Genome length (bp)    	|     Number of artificial reads added    	|     Expected average number of reads per position     	|     Expected proportion of viral reads (%)    	|
|-------------------	|-------------------	|---------------------------	|-----------------------------------------	|-------------------------------------------------------	|-----------------------------------------------	|
|     AB711147      	|     Artificial    	|     9672                  	|     39552                               	|     1187                                              	|     37.79                                     	|
|     KC634004      	|     Artificial    	|     9650                  	|     19776                               	|     593                                               	|     19.63                                     	|
|     MF176828      	|     Artificial    	|     9673                  	|     14784                               	|     443                                               	|     18.89                                     	|
|     JQ969039      	|     Artificial    	|     9689                  	|     20544                               	|     616                                               	|     19.63                                     	|
|     FJ214726      	|     Artificial    	|     9773                  	|     10010                               	|     300                                               	|     9.56                                      	|                                            	|     9.91                                      	|


*Table 2: Percentage identity between the strains.*

|                       	|     MF176828.1    	|     KC634004.1    	|     JQ969039.2    	|     FJ214726.1    	|     AB711147.1    	|
|-----------------------	|-------------------	|-------------------	|-------------------	|-------------------	|-------------------	|
|     **MF176828.1**    	|     100           	|     90.377        	|     86.175        	|     81.131        	|     83.699        	|
|     **KC634004.1**    	|     90.377        	|     100           	|     92.181        	|     81.746        	|     88.209        	|
|     **JQ969039.2**    	|     86.175        	|     92.181        	|     100           	|     82.73         	|     93.784        	|
|     **FJ214726.1**    	|     81.131        	|     81.746        	|     82.73         	|     100           	|     83.528        	|
|     **AB711147.1**    	|     83.699        	|     88.209        	|     93.784        	|     83.528        	|     100           	|
